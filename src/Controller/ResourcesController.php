<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Datasource\ConnectionManager;

/**
 * Resources Controller
 */
class ResourcesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $page = 1;
        $limit = 20;
        $orderBy = 'id';
        $order = 'ASC';
        $params = [];
        if($this->request->is('get')){
            $params = $this->request->query;
        }else{
            $params = $this->request->data;
        }

        if(!empty($params['page'])){
            $page = $params['page'];
        }
        if(!empty($params['limit'])){
            $limit = $params['limit'];
        }
        if(!empty($params['order_by'])){
            $orderBy = $params['order_by'];
        }
        if(!empty($params['order'])){
            $order = $params['order'];
        }
        if (strpos($orderBy, '.') !== true) {
            $orderBy = 'Resources.'.$orderBy;
        }

        $conn = ConnectionManager::get("default");

        $query = $conn->newQuery();
        $query->select(['Locations.*', 'Managers.*', 'Resources.*'])
            ->from('resources as Resources')
            ->join([
                'table' => 'locations',
                'alias' => 'Locations',
                'type' => 'LEFT',
                'conditions' => 'Locations.resource_id = Resources.id'
            ])
            ->join([
                'table' => 'managers',
                'alias' => 'Managers',
                'type' => 'LEFT',
                'conditions' => 'Resources.manager_id = Managers.id'
            ])
            ->where()
            ->limit($limit)
            ->order([$orderBy => $order])
            ->page($page);
        $stmt = $query->execute();
        $rows = $stmt->fetchAll('assoc');
        return $this->_jsonResponse($rows);
    }

    /**
     * View method
     *
     * @param string|null $id Resource id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $conn = ConnectionManager::get("default");

        $query = $conn->newQuery();
        $query->select(['Locations.*', 'Managers.*', 'Resources.*'])
            ->from('resources AS Resources')
            ->join([
                'table' => 'locations',
                'alias' => 'Locations',
                'type' => 'LEFT',
                'conditions' => 'Locations.resource_id = Resources.id'
            ])
            ->join([
                'table' => 'managers',
                'alias' => 'Managers',
                'type' => 'LEFT',
                'conditions' => 'Resources.manager_id = Managers.id'
            ])
            ->where(['Resources.id' => $id])
            ->limit(1);
        $stmt = $query->execute();

        $rows = $stmt->fetch('assoc');
        return $this->_jsonResponse($rows);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->request->is('post')) {
            $errors = [];
            $data = $this->request->getData();
/*
            $data['location']['latitude'] = $data['latitude'];
            $data['location']['longitude'] = $data['longitude'];*/

            if(!isset($data['location']['latitude']) || empty($data['location']['latitude']))
            {
                $errors[] = [
                    'latitude' => ['_empty' => __("This field cannot be left empty")]
                ];
            }
            if(!isset($data['location']['longitude']) || empty($data['location']['longitude']))
            {
                $errors[] = [
                    'longitude' => ['_empty' => __("This field cannot be left empty")]
                ];
            }
            if(!isset($data['name']) || empty($data['name']))
            {
                $errors[] = [
                    'name' => ['_empty' => __("This field cannot be left empty")]
                ];
            }

            if(empty($errors)){
                $conn = ConnectionManager::get("default");

                $query = $conn->newQuery();

                $conn->transactional(function ($conn) use ($data) {
                    $query = $conn->newQuery();
                    $queryLocations = $conn->newQuery();
                    $query
                        ->insert(['name', 'created', 'modified'])
                        ->into('resources')
                        ->values(['name' => $data['name'],
                            'created' => date("Y-m-d H:i:s"),
                            'modified' => date("Y-m-d H:i:s")
                        ]);
                    $stmt = $query->execute();
                    $queryLocations
                        ->insert(['resource_id', 'latitude', 'longitude'])
                        ->into('locations')
                        ->values([
                            'resource_id' => $stmt->lastInsertId('resources'),
                            'latitude' => $data['location']['latitude'],
                            'longitude' => $data['location']['longitude']]);
                    $stmtLocations = $queryLocations->execute();
                });
                return $this->_jsonResponse($data, false, __('The resource has been saved.'));
            }

            return $this->_jsonResponse($data, true,
                __('The resource could not be saved. Please, try again.'),
                $errors);
        }

        return $this->_jsonResponse([]);
    }

    /**
     * Edit method
     *
     * @param string|null $id Resource id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $conn = ConnectionManager::get("default");

        $query = $conn->newQuery();
        $query->select(['*', 'Locations.id as location_id'])
            ->from('resources')
            ->join([
                'table' => 'locations',
                'alias' => 'Locations',
                'type' => 'LEFT',
                'conditions' => 'Locations.resource_id = resources.id'
            ])
            ->join([
                'table' => 'managers',
                'alias' => 'Managers',
                'type' => 'LEFT',
                'conditions' => 'resources.manager_id = Managers.id'
            ])
            ->where(['resources.id' => $id])
            ->limit(1);
        $stmt = $query->execute();
        $resource = $stmt->fetchAll('assoc');

        if(empty($resource)){
            return $this->_jsonResponse([], true, __('The resource does not exists.'));
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $errors = [];
            $data = $this->request->getData();

            $data['location']['latitude'] = $data['latitude'];
            $data['location']['longitude'] = $data['longitude'];

            if(!isset($data['location']['latitude']) || empty($data['location']['latitude']))
            {
                $errors[] = [
                    'latitude' => ['_empty' => __("This field cannot be left empty")]
                ];
            }
            if(!isset($data['location']['longitude']) || empty($data['location']['longitude']))
            {
                $errors[] = [
                    'longitude' => ['_empty' => __("This field cannot be left empty")]
                ];
            }
            if(!isset($data['name']) || empty($data['name']))
            {
                $errors[] = [
                    'name' => ['_empty' => __("This field cannot be left empty")]
                ];
            }

            if(empty($errors)){
                $conn = ConnectionManager::get("default");
                $conn->transactional(function ($conn) use ($data, $resource) {
                    $query = $conn->newQuery();
                    $queryLocations = $conn->newQuery();
                    $query
                        ->update('resources')
                        ->set([
                            'name' => $data['name'],
                            'modified' => date("Y-m-d H:i:s")
                        ])
                        ->where(['id' => $resource[0]['resource_id']]);
                    $stmt = $query->execute();
                    $queryLocations
                        ->update('locations')
                        ->set(['latitude' => $data['location']['latitude'],
                            'longitude' => $data['location']['longitude']])
                        ->where(['id' => $resource[0]['location_id']]);
                    $stmtLocations = $queryLocations->execute();
                });
                return $this->_jsonResponse($data, false, __('The resource has been saved.'));
            }

            return $this->_jsonResponse($data, true,
                __('The resource could not be saved. Please, try again.'),
                $errors);
        }

        return $this->_jsonResponse([]);
    }

    /**
     * Delete method
     *
     * @param string|null $id Resource id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);

        $conn = ConnectionManager::get("default");

        $query = $conn->newQuery();
        $query->select(['*', 'Locations.id as location_id'])
            ->from('resources')
            ->join([
                'table' => 'locations',
                'alias' => 'Locations',
                'type' => 'LEFT',
                'conditions' => 'Locations.resource_id = resources.id'
            ])
            ->join([
                'table' => 'managers',
                'alias' => 'Managers',
                'type' => 'LEFT',
                'conditions' => 'resources.manager_id = Managers.id'
            ])
            ->where(['resources.id' => $id])
            ->limit(1);
        $stmt = $query->execute();
        $resource = $stmt->fetchAll('assoc');

        if(empty($resource)){
            return $this->_jsonResponse([], true, __('The resource does not exists.'));
        }

        $conn->transactional(function ($conn) use ($resource) {
            $query = $conn->newQuery();
            $queryLocations = $conn->newQuery();
            $query
                ->delete('resources')
                ->where(['id' => $resource[0]['resource_id']]);
            $stmt = $query->execute();
            $queryLocations
                ->delete('locations')
                ->where(['id' => $resource[0]['location_id']]);
            $stmtLocations = $queryLocations->execute();
        });

        return $this->_jsonResponse([], false, __('The resource has been deleted.'));

    }
}
