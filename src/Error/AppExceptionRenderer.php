<?php
namespace App\Error;

use Cake\Error\ExceptionRenderer;

class AppExceptionRenderer extends ExceptionRenderer
{
    protected function _outputMessage($template)
    {
        $this->controller->RequestHandler->renderAs($this->controller, 'json');
        $this->controller->response->type('application/json');
        $this->controller->set('_serialize', true);

        return parent::_outputMessage($template);
    }

}